---
layout: handbook-page-toc
title: "Records Retention & Disposal"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Records Retention & Disposal

## Purpose 
This GitLab records retention and disposal standard lists the specific retention and secure disposal requirements for critical GitLab records. These minimum requirements inform design and maintenance decisions for all GitLab [tier 1 and tier 2 critical systems](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-risk/storm-program/critical-systems.html).

## Scope
The below retention and secure disposal requirements apply to all GitLab records enumerated in the table below stored in GitLab [tier 1 and tier 2 critical systems](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-risk/storm-program/critical-systems.html).

## Roles & Responsibilities:

| Role  | Responsibility | 
|-----------|-----------|
| Security Compliance Team | Responsible for reviewing and maintaining this controlled document. | 
| Security Assurance Management | Responsible for approving changes to this controlled document. |
| Control Owners | Responsible for defining and implementing procedures to support the below requirements. | 

## Retention & Disposal Requirements

| Record                                             | Retention Requirement     | Disposal Requirement |
|----------------------------------------------------|---------------------------|----------------------|
| Business continuity plan approvals                 | 3 years                   | [GCP Secure Deletion] |
| Business continuity test results                   | 3 years                   | [GCP Secure Deletion] |
| Production backup tests                            | 1 year                    | [GCP Secure Deletion] |
| Production changes                                 | 3 years                   | [GCP Secure Deletion] |
| Security policy review/approvals                   | 3 years                   | [GCP Secure Deletion] |
| Terms of service acceptance                        | As long as user is active |  [GCP Secure Deletion] |
| Access request/change records                      | 1 year                    |  [GCP Secure Deletion] |
| Team-member offboarding issues                     | 1 year                    |  [GCP Secure Deletion] |
| System access reviews                              | 1 year 3 months           |  [GCP Secure Deletion] |
| Shared and group authentication reviews            | 1 year 3 months           |   [GCP Secure Deletion] |
| Production audit logs                              | 1 year                    |   [GCP Secure Deletion] |
| GCP firewall configuration artifacts               | 1 year                    |   [GCP Secure Deletion] |
| Job roles and responsibilities                     | 1 year                    |   [GCP Secure Deletion] |
| Security incident communication to customers       | 3 years                   |   [GCP Secure Deletion] |
| Background check results                           | 6 years                   |  [GCP Secure Deletion] |
| 1:1 meeting notes                                  | 6 years                   |  [GCP Secure Deletion] |
| On-boarding tickets                                | 1 year                    | [GCP Secure Deletion] |
| Annual risk assessment report                      | 2 years                   |    [GCP Secure Deletion] |
| Risk treatment plans                               | 3 years                    |  [GCP Secure Deletion] |
| Security observation issues                        | 3 years                    |   [GCP Secure Deletion] |
| Board of Directors meeting minutes                 | Indefinite                | N/A                  |
| Release notes                                      | 1 year                    |  [GCP Secure Deletion] |
| Critical system activity logs                      | 60 days                   | [GCP Secure Deletion] |
| Security monitoring alerts/metrics                 | 3 years                   | [GCP Secure Deletion] |
| Vendor security review issues                      | 3 years                   | [GCP Secure Deletion] |
| Customer-signed MSA's                              | Indefinite                | N/A                  |
| Vendor NDA's                                       | Indefinite                | N/A                  |
| Annual security awareness training records         | 3 years                   | [GCP Secure Deletion] |
| Code of conduct review records                     | 3 years                   | [GCP Secure Deletion] |
| Secure coding training records                     | 2 years                   | [GCP Secure Deletion] |
| Penetration testing reports and remediation issues | 2 years                   | [GCP Secure Deletion] |
| System patch records                               | 1 year                    | [GCP Secure Deletion] |
| Source code scanning results                       | 1 year                    | [GCP Secure Deletion] |
| ZenDesk tickets                                    | 1 year                    | [GCP Secure Deletion] |
| Nonpublic information review records               | 3 years                   | [GCP Secure Deletion] |
| Role-based security training records               | 3 years                   | [GCP Secure Deletion] |
| Audit log review records                           | 3 years                   | [GCP Secure Deletion] |
| Security assessment reports/observation            | 3 years                   | [GCP Secure Deletion] |
| Security configuration reviews/alerts              | 3 years                   | [GCP Secure Deletion] |
| Security authorization records                     | 3 years                   | [GCP Secure Deletion] |
| System connection requirements                     | 3 years                   | [GCP Secure Deletion] |
| Configuration change records                       | 3 years                   | [GCP Secure Deletion] |
| Security impact analysis records                   | 3 years                   | [GCP Secure Deletion] |
| Production asset inventory                         | 3 years                   | [GCP Secure Deletion] |
| BC training records                                | 3 years                   | [GCP Secure Deletion] |
| Production backups                                 | Organizationally-defined  | [GCP Secure Deletion] |
| Customer data backups                              | Organizationally-defined  | [GCP Secure Deletion] |
| Employment applications and interview notes of US-based applicants| 4 years (updated 2021-07)|N/A      | 

## Exceptions
Exceptions to these requirements will be tracked as per the [Information Security Policy Exception Management Process](/handbook/engineering/security/#information-security-policy-exception-management-process).

## References
- [Controlled Document Procedure](/handbook/engineering/security/controlled-document-procedure.html)
- [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html)
- [Google Cloud Platform data deletion](https://cloud.google.com/security/deletion)

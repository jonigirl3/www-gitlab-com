title: ANWB
cover_image: '/images/blogimages/anwb_case_study_image_2.jpg'
cover_title: |
  From Bicycles to Connected Driving
cover_description: |
  ANWB has innovated travel for the past 135 years and is actively preparing for the next 135
canonical_path: /customers/anwb/
twitter_image: '/images/blogimages/hemmersbach_case_study.jpg'
file_name: anwb
customer_logo: '/images/case_study_logos/ANWB.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Roadside assistance / Travel / Insurances / Credit Cards / Leisure / Retail
customer_location: Netherlands
customer_solution: Premium
customer_employees: |
   3,700 employees
customer_overview: |
   ANWB is the largest club in the Netherlands and works to keep its 4.4 million members on the road.
customer_challenge: |
  The ANWB web and mobile team was struggling with an outdated toolchain that was pieced together with plugins and prayers.

key_benefits: >-
    Performing regular deploys


    Everyone is contributing to each other's code repositories


    Improved isolation of CI/CD pipelines


    Increased team autonomy

customer_stats:
  - stat: 100%
    label: dev teams using GitLab
  - stat: 322
    label: projects
  - stat: 57
    label: groups
customer_study_content:
  - title: the customer
    subtitle: Keeping the Netherlands rolling
    content: >-
       What started as a small community of bike enthusiasts has grown into a full-service
       mobility provider over the past 135 years. The Royal Dutch Touring Club, or Algemene Nederlandse
       Wielrijdersbond, was formed in 1883 when a few local cycling clubs merged. With the evolution of
       automobiles, the club shifted focus to roadside service. Nowadays, next to roadside services ANWB offers
       services around credit cards, car sales, bicycle maintenance, and travel services.


       Some of the club’s most popular services include route planning software for mobile devices and connected
       car service which enables older cars to provide intelligence to the drivers.



  - title: the challenge
    subtitle: Replacing multi-tools and plugins with a unified toolset
    content: >-
       At the beginning of 2018, the product and development teams decided to make a strategic change to improve
       their development process. At that time, they had multiple interconnected tooling and services. This environment
       was driven by developer and administrator’s knowledge and background and wasn’t easily maintained. System breakdowns
       required someone to manually look at what was going on. These prolonged outages prevented people from doing their
       jobs properly. The company operated with an attitude of  "Okay, it's working. Don't touch. As long as we don't
       touch it, everything will be fine."


        The toolchain consisted of some outdated tools including Jenkins version 1 as a build server, Stash (now known
        as Bitbucket Server) as a Git repository server. For artifact hosting, ANWB was using Artifactory and Nexus.

  - title: the solution
    subtitle: Empowering developers with a one-stop, modern toolchain
    content: >-
        ANWB wanted to increase team autonomy and obliterate process isolation. Previously, their teams were using
        pipelines that were pieced together with various plugins. Every once in a while, a plug-in would break after
        a Jenkins update. Because of this, pipelines were kept as simple as possible. Now with some process shifts and
        using GitLab, developers run their jobs in separate pipelines and can upgrade in their own time.


        GitLab has made it easier for ANWB to manage separate teams and their individualized projects and for teams
        to choose their own pipelines and their own processes. Having the ability to choose their own workflows prevents
        teams from having to conform to a single way of working.

  - title: the results
    subtitle: Looking to the future and the potential of a multi-cloud environment
    content: >-
        With sights set firmly on the future, The ANWB dev team is looking to migrate from outdated backend architecture
        to more of a cloud-centric framework. While the path to accomplishing this hasn’t been defined yet, they know
        GitLab is key to this next step.


        “What we see is that GitLab really helps in seeing what's out there because of the integrations that GitLab
        already provides out-of-the-box,” Crefcoeur said. “Our company, ANWB, uses both AWS and Azure but our department is not making use of those services yet. What we are doing is we're currently experimenting with Google Cloud platform to see what GitLab has to offer on that.”


        Because of GitLab’s integrations with Knative, ANWB is exploring Google Cloud on Kubernetes.


        “We think that Kubernetes is the way to go and the integration with GitLab is really helping on that one.
        I think the findings are rather positive for now but we still have a long way to go because it's pretty huge
        what you can do with all those new techniques” van de Velde said.
customer_study_quotes:
  - blockquote: We had developers that thought, Why would we do something else? Jenkins is fine. But I think those people need to see GitLab first and see what the difference is because GitLab is so much more than Jenkins. The power of GitLab is you can do so much more and you can make everything so much easier to manage.
    attribution: Michiel Crefcoeur
    attribution_title: Frontend Build and Release Engineer
  - blockquote: We are just replacing these outdated tools bit by bit, doing it the agile way. We really wanted to replace Jenkins and immediately we saw that Stash was the first one to replace because it was logical to also host your Git repositories in GitLab, and now we have started looking at other tools (f.i. XL Deploy, Nexus) that we can replace with GitLab.
    attribution: Ramon van de Velde
    attribution_title: Product Owner Beheer | P&I Online


